#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: instruction
# @Author: RyneZ
# @Time: 2022-10-01 13:31
from PyQt5.QtWidgets import *
from UIDisplay.instruction_ui import *
import logging

class instructionWindow(QWidget, Ui_instructionForm):
    def __init__(self):
        super(instructionWindow, self).__init__()
        self.setupUi(self)



    def open(self):
        self.show()
        self.setFixedSize(self.width(), self.height())
        self.initializecontent()

    def initializecontent(self):
        mdstr=''
        try:
            with open('README.md',encoding='UTF-8') as f:
                for line in f.readlines():
                    mdstr=mdstr+line
        except FileNotFoundError:
            logging.exception('未找到readme文件')
        self.instructionBrowser.setMarkdown(mdstr)


