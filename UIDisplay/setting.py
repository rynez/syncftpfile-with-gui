#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: setting
# @Author: RyneZ
# @Time: 2022-09-26 20:20

import logging
import os
import xml.dom.minidom
from PyQt5.QtWidgets import *
from UIDisplay.syncftpfile_settingui import *

class setWindow(QWidget, Ui_configForm):
    def __init__(self):
        super(setWindow, self).__init__()
        self.setupUi(self)
        self.initializesetting()
        self.selectButton.clicked.connect(self.selectButton_Clicked)
        self.saveButton.clicked.connect(self.saveButton_Clicked)


    def open(self):
        self.show()
        self.setFixedSize(self.width(), self.height())

    def initializesetting(self):
        if not os.path.exists("config.xml"):
            self.ftpaddr=''
            self.port=''
            self.encoding=''
            self.username=''
            self.pwd=''
            self.localfolder=''
            return 0
        else:
            try:
                config_obj = xml.dom.minidom.parse("config.xml")
            except:
                logging.error('未找到config.xml文件')
                self.ftpaddr = ''
                self.port = ''
                self.encoding = ''
                self.username = ''
                self.pwd = ''
                self.localfolder = ''
                return 0
            try:
                ipaddrnode=config_obj.getElementsByTagName("ipaddr")
                self.ftpEdit.setText(ipaddrnode[0].childNodes[0].data)
            except IndexError:
                self.ftpEdit.setText('')
            try:
                portnode = config_obj.getElementsByTagName("port")
                self.portEdit.setText(portnode[0].childNodes[0].data)
            except IndexError:
                self.portEdit.setText('')
            try:
                usernode = config_obj.getElementsByTagName("user")
                self.userEdit.setText(usernode[0].childNodes[0].data)
            except IndexError:
                self.userEdit.setText('')
            try:
                passnode = config_obj.getElementsByTagName("pass")
                self.pwdEdit.setText(passnode[0].childNodes[0].data)
            except:
                self.pwdEdit.setText('')
            try:
                encodingnode = config_obj.getElementsByTagName("encoding")
                if encodingnode[0].childNodes[0].data=='gb2312':
                    self.encodeBox.setCurrentIndex(0)
                elif encodingnode[0].childNodes[0].data=='UTF-8':
                    self.encodeBox.setCurrentIndex(1)
            except IndexError:
                self.encodeBox.setCurrentIndex(0)
            try:
                localdirnode = config_obj.getElementsByTagName("localdir")
                self.folderEdit.setText(localdirnode[0].childNodes[0].data)
            except IndexError:
                self.folderEdit.setText('./')
            #self.ftpEdit.setText(config_obj.documentElement.nodeName)

    def selectButton_Clicked(self):
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "getExistingDirectory", "./")
        self.folderEdit.setText(directory)

    def saveButton_Clicked(self):
        config=xml.dom.minidom.Document()
        root=config.createElement('cfg')
        config.appendChild(root)

        ipaddrnode=config.createElement('ipaddr')
        ipaddrnode.appendChild(config.createTextNode(self.ftpEdit.text()))

        portnode=config.createElement('port')
        portnode.appendChild(config.createTextNode(self.portEdit.text()))

        usernode = config.createElement('user')
        usernode.appendChild(config.createTextNode(self.userEdit.text()))

        passnode = config.createElement('pass')
        passnode.appendChild(config.createTextNode(self.pwdEdit.text()))

        encodenode=config.createElement('encoding')
        if self.encodeBox.currentIndex()==0:
            encodetype='gb2312'
        elif self.encodeBox.currentIndex()==1:
            encodetype='UTF-8'
        encodenode.appendChild(config.createTextNode(encodetype))

        localdirnode = config.createElement('localdir')
        if self.folderEdit.text().endswith('/') or self.folderEdit.text().strip()=='':
            localdirnode.appendChild(config.createTextNode(self.folderEdit.text()))
        else:
            localdirnode.appendChild(config.createTextNode(self.folderEdit.text()+'/'))
        root.appendChild(ipaddrnode)
        root.appendChild(portnode)
        root.appendChild(usernode)
        root.appendChild(passnode)
        root.appendChild(encodenode)
        root.appendChild(localdirnode)
        try:
            with open('config.xml','w',encoding='utf-8') as f:
                config.writexml(f,indent='\t',addindent='\t',newl='\n',encoding='utf-8')
        except:
            logging.exception('保存文件出错，config.xml文件在打开状态或者文件权限存在问题')