#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: main
# @Function:
# @Author: RyneZ
# @Time: 2021/9/8 12:49

import sqlite3
from datetime import datetime
import sys
from sys import exit
import re
import xmltodict
import shutil
import ftplib
import logging
from ftplib import FTP
from PyQt5.QtCore import pyqtSignal, QRect,Qt,QThread,QObject
from PyQt5.QtGui import *
from syncftpfile_mainui import *
from UIDisplay.__init__ import *

all_header_combobox = []
header_field = ['全选', '规则库类别', '远程ftp目录', '新版本文件(夹)名','是否需要更新']
class CheckBoxHeader(QHeaderView):
    """自定义表头类"""

    # 自定义 复选框全选信号
    select_all_clicked = pyqtSignal(bool)
    # 这4个变量控制列头复选框的样式，位置以及大小
    _x_offset = 0
    _y_offset = 0
    _width = 20
    _height = 20

    def __init__(self, orientation=Qt.Horizontal, parent=None):
        super(CheckBoxHeader, self).__init__(orientation, parent)
        self.isOn = False

    def paintSection(self, painter, rect, logicalIndex):
        painter.save()
        super(CheckBoxHeader, self).paintSection(painter, rect, logicalIndex)
        painter.restore()

        self._y_offset = int((rect.height() - self._width) / 2.)

        if logicalIndex == 0:
            option = QStyleOptionButton()
            option.rect = QRect(rect.x() + self._x_offset, rect.y() + self._y_offset, self._width, self._height)
            option.state = QStyle.State_Enabled | QStyle.State_Active
            if self.isOn:
                option.state |= QStyle.State_On
            else:
                option.state |= QStyle.State_Off
            self.style().drawControl(QStyle.CE_CheckBox, option, painter)

    def mousePressEvent(self, event):
        index = self.logicalIndexAt(event.pos())
        if 0 == index:
            x = self.sectionPosition(index)
            if x + self._x_offset < event.pos().x() < x + self._x_offset + self._width and self._y_offset < event.pos().y() < self._y_offset + self._height:
                if self.isOn:
                    self.isOn = False
                else:
                    self.isOn = True
                    # 当用户点击了行表头复选框，发射 自定义信号 select_all_clicked()
                self.select_all_clicked.emit(self.isOn)

                self.updateSection(0)
        super(CheckBoxHeader, self).mousePressEvent(event)

    # 自定义信号 select_all_clicked 的槽方法
    def change_state(self, isOn):
        # 如果行表头复选框为勾选状态
        if isOn:
            # 将所有的复选框都设为勾选状态
            for i in all_header_combobox:
                i.setCheckState(Qt.Checked)
        else:
            for i in all_header_combobox:
                i.setCheckState(Qt.Unchecked)

class checkthread(QObject):
    _signal=pyqtSignal(int)#进度条信号
    #checkresult=pyqtSignal(str)#检查结果
    finished=pyqtSignal(dict)
    def __init__(self,ftp):
        super(checkthread, self).__init__()
        self.ftpcon=ftp
        #self.files=files

    def run(self):
        try:
            self.tableinfos = {}
            self.files = self.ftpcon.getFile(self._signal)
            i = 0
            try:
                for file in self.files:
                    fileName, localFile, serFile, localDir = self.ftpcon.getNewestFile(file[0], file[1])
                    self.tableinfos[i] = [file[1], fileName, localFile, serFile, localDir]
                    i = i + 1
                    #self._signal.emit(int(i*100/len(self.files)))
            except TypeError:
                logging.exception('未获取到远程文件列表')
                self.ftpcon.ftp.quit()
                return 0
            self.ftpcon.ftp.quit()
            self.finished.emit(self.tableinfos)


        except ValueError:
            print(file[0],file[1])


class downthread(QObject):
    #  通过类成员对象定义信号对象
    _signal = pyqtSignal(int)#进度条信号
    finished = pyqtSignal()#是否运行完成信号
    noticesig= pyqtSignal(str)#运行信息信号
    curfilesig=pyqtSignal(str)#当前下载文件
    def __init__(self,ftp,tableinfos, all_header_combobox,customizefolder):
        super(downthread, self).__init__()
        #self.ftpcon = FTPSync(self.ipaddr, self.port, self.user, self.passwd, self.encoding, self.dir)
        self.isstop=0
        self.FTP=ftp
        self.tableinfos=tableinfos
        self.customizefolder=customizefolder
        self.all_header_combobox=all_header_combobox
        self.FTP.ftpConnect()

    def pause(self):
        print("线程休眠")
        self._isPause = True

    def stop(self):
        self.file.close()
        #self.FTP.close()
        #exit()

    def run(self):
        for i in range(len(self.all_header_combobox)):
            if self.all_header_combobox[i].checkState() == 2:

                if self.customizefolder:
                    localDir=self.customizefolder
                else:
                    localDir = self.tableinfos[i][4]
                if self.customizefolder:
                    localFile=self.tableinfos[i][2].replace(self.tableinfos[i][4],self.customizefolder)
                else:
                    localFile = self.tableinfos[i][2]
                serverFile = self.tableinfos[i][3]

                if not os.path.exists(localDir):
                    os.makedirs(localDir)
                if os.path.exists(localFile) and os.path.getsize(localFile):
                    self.noticesig.emit('本地存在非空文件'+localFile+'，已忽略')
                    continue
                self.curfilesig.emit(serverFile)
                self.bufsize = 1024
                self.datelen = 0
                self.size = self.FTP.ftp.size(serverFile)
                self.file = open('./temporary.file', 'wb')
                try:
                    self.FTP.ftp.retrbinary("RETR " + serverFile, self.file_write, self.bufsize)
                except ftplib.error_temp:
                    return
                except ValueError:
                    return
                except AttributeError:
                    return
                self.file.close()
                shutil.move('./temporary.file',localFile)
                self.noticesig.emit('已下载' + localFile )
                logging.info('已成功下载：'+localFile)
        #self.FTP.close()
        self.finished.emit()



    def file_write(self,data):  # 在ftp下载函数中调用
        if not self.isstop:
            self.datelen = self.datelen + len(data)
            self.file.write(data)
            self._signal.emit(int(self.datelen / self.size * 100))
        else:
            self.file.close()
            exit()



class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        # 初始化UI

        self.setupUi(self)
        self.setup=0
        self.downloading=0
        self.status = self.statusBar()
        self.status.showMessage('ver 106 @RyneZ')
        self._isStop = False
        self.stopButton.setEnabled(False)
        self.downloadButton.setEnabled(False)
        self.cleanoldButton.setEnabled(False)
        self.oncedownButton.setEnabled(False)
        self.tableinfos={}#存放所有规则库信息
        self.selectedinfos={}#存放下拉框选择的规则库信息
        self.devices={}
        logging.info('主窗口创建成功')
        self.dbcon=sqlite3.connect('remoteinfos.db',check_same_thread=False)
        self.dbcursor = self.dbcon.cursor()
        logging.info('本地数据库连接成功')
        self.showDevices()#初始化下拉框
        self.setTableView()#初始化列表
        self.checknewfileBotton.clicked.connect(self.checkBottonClicked)
        self.downloadButton.clicked.connect(lambda:self.downloadBottonClicked(True))
        self.oncedownButton.clicked.connect(lambda:self.downloadBottonClicked(False))
        self.stopButton.clicked.connect(self.stopThread)
        self.cleanoldButton.clicked.connect(self.cleanoldButtonClicked)
        self.alldevicesBox.currentIndexChanged.connect(self.alldevicesBoxChanged)



    def closeEvent(self, event):
        notice = QMessageBox.question(
            self,
            '提示',
            '是否要关闭所有窗口？未保存内容将会丢失。',
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No)
        if notice == QMessageBox.Yes:
            event.accept()
            sys.exit(0)
        else:
            event.ignore()


    def setTableView(self):

        # if self.tableinfos:

        self.tableWidget.setColumnCount(len(header_field))
        self.tableWidget.setRowCount(len(self.selectedinfos))
        if self.setup==0:
            self.setTableHeaderField()  # 设置表格行表头字段
        else:
            self.tableWidget.setHorizontalHeaderLabels(header_field)  # 设置行表头字段
        self.tableWidget.clearContents()

        global all_header_combobox
        all_header_combobox=[]
        for i in range(len(self.selectedinfos)):
            #header_item = QTableWidgetItem(header_field[i])
            checkbox = QCheckBox()
            # 将所有的复选框都添加到 全局变量 all_header_combobox 中
            all_header_combobox.append(checkbox)
            # 为每一行添加复选框
            self.tableWidget.setCellWidget(i, 0, checkbox)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableWidget.horizontalHeader().setSectionResizeMode(1, QHeaderView.Interactive)
        #self.tableWidget.horizontalHeader().setSectionResizeMode(2, QHeaderView.ResizeToContents)
        self.tableWidget.horizontalHeader().setSectionResizeMode(3, QHeaderView.ResizeToContents)
        self.tableWidget.horizontalHeader().setSectionResizeMode(4, QHeaderView.Interactive)
        self.tableWidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)#第0列固定
        self.tableWidget.setColumnWidth(0, 60)  # 设置第0列宽度
        self.setTableContents()
        self.tableWidget.setAlternatingRowColors(True)  # 交替行颜色

    #全选
    def setTableHeaderField(self):
        self.setup=1
        # self.tableWidget.setColumnCount(len(header_field))  # 设置列数
        header = CheckBoxHeader()  # 实例化自定义表头
        self.tableWidget.setHorizontalHeader(header)  # 设置表头
        self.tableWidget.setHorizontalHeaderLabels(header_field)  # 设置行表头字段
        header.select_all_clicked.connect(header.change_state)  # 行表头复选框单击信号与槽

    # 设置表格内容，根据实际情况设置即可
    def setTableContents(self):
        if self.selectedinfos:
            for i,content in self.selectedinfos.items():
                #规则库类别
                self.tableWidget.setItem(i,1,QTableWidgetItem(str(content[0][0])))
                #远程文件目录
                self.tableWidget.setItem(i, 2, QTableWidgetItem(str(content[0][1])))
                #待下载新文件名
                self.tableWidget.setItem(i, 3, QTableWidgetItem(str('未在服务器找到该规则库文件' if content[1]==0 else content[1])))
                #是否需要更新
                if content[2]==0:
                    self.tableWidget.setItem(i, 4,
                                             QTableWidgetItem(str('')))
                else:
                    self.tableWidget.setItem(i, 4, QTableWidgetItem(str('是' if os.path.exists(content[2])==0 else '否')))

            #print(content[0][0],content[0][1],content[1],os.path.exists(content[2]))


    def readConfigFile(self):
        try:
            with open('config.xml', encoding='utf-8') as configfile:
                self.xmlcontentstr = configfile.read()
            self.serverdict = xmltodict.parse(self.xmlcontentstr)
        except Exception as E:
            logging.exception(
                "ftp server information config file not found")
            self.showDialog('warning',"读取配置文件:config.xml失败，程序将退出，请检查文件是否存在或者是否符合格式要求")
        self.ipaddr = self.serverdict['cfg']['ipaddr']
        self.port = self.serverdict['cfg']['port']
        self.user = self.serverdict['cfg']['user']
        self.passwd = self.serverdict['cfg']['pass']
        self.encoding = self.serverdict['cfg']['encoding']
        self.dir = self.serverdict['cfg']['localdir']

    def showDialog(self, dialogtype, warnstr):

        if dialogtype == 'warning':
            QMessageBox.warning(self, '警告', warnstr)

        if dialogtype == 'information':
            QMessageBox.information(self, '信息', warnstr)


    ###槽函数

    def checkBottonClicked(self):
        self.checknewfileBotton.setEnabled(False)
        self.downloadButton.setEnabled(False)
        self.cleanoldButton.setEnabled(False)
        self.oncedownButton.setEnabled(False)
        self.filenamelabel.setText('检查更新中...')
        self.readConfigFile()

        self.ftpcon = FTPSync(self.ipaddr, self.port, self.user, self.passwd, self.encoding, self.dir,self.dbcon)
        try:
            self.ftpcon.ftpConnect()
        except TimeoutError as e:
            self.checknewfileBotton.setEnabled(True)
            self.showDialog('warning','未能连接至服务器，请检查地址输入是否正确')
            logging.warning("can't connect to ftp server:"+str(e))
            return 0
        except ftplib.error_perm as e:
            self.checknewfileBotton.setEnabled(True)
            self.showDialog('warning', '未能连接至服务器，请检查用户名密码是否正确')
            logging.warning("can't connect to ftp server:"+str(e))
            return 0
        self.checkthreadact = checkthread(self.ftpcon)
        self.mycheckthread = QThread()
        # 开始线程
        self.checkthreadact.moveToThread(self.mycheckthread)

        self.mycheckthread.started.connect(self.checkthreadact.run)
        self.checkthreadact.finished.connect(self.checkfinished)
        self.mycheckthread.start()
        self.checkthreadact._signal.connect(self.updateProcessbar)
        #self.checkthreadact.finished.connect(self.checkfinished)
        # try:
        #     self.ftpcon.ftpConnect()
        #     self.tableinfos = {}
        #     self.files = self.ftpcon.getFile()
        #     i=0
        #     try:
        #         for file in self.files:
        #             fileName,localFile,serFile,localDir=self.ftpcon.getNewestFile(file[0], file[1])
        #             self.tableinfos[i]=[file[1],fileName,localFile,serFile,localDir]
        #             i=i+1
        #     except TypeError:
        #         logging.exception('未获取到远程文件列表')
        #         self.ftpcon.ftp.quit()
        #         return 0
        #     self.ftpcon.ftp.quit()
        #
        #     self.alldevicesBox.setCurrentIndex(0)
        #     self.selectedinfos = self.tableinfos
        #     self.setTableView()
        #
        #     self.downloadButton.setEnabled(True)
        #     self.cleanoldButton.setEnabled(True)
        #     self.oncedownButton.setEnabled(True)
        # except TimeoutError as e:
        #     self.showDialog('warning','未能连接至服务器，请检查地址输入是否正确')
        #     logging.warning("can't connect to ftp server:"+str(e))
        # except ftplib.error_perm as e:
        #     self.showDialog('warning', '未能连接至服务器，请检查用户名密码是否正确')
        #     logging.warning("can't connect to ftp server:"+str(e))
        #
        # except ValueError:
        #     print(file[0],file[1])
        # print('--------------------')


    def downloadBottonClicked(self,defaultdown):
        #self.ftpcon.ftpConnect()
        self._isStop=False
        self.progressBar.reset()
        #将连接和选择框传递给线程处理
        self.ftpcon = FTPSync(self.ipaddr, self.port, self.user, self.passwd, self.encoding, self.dir,self.dbcon)
        self.ftpcon.ftpConnect()
        if defaultdown:
            self.threadact = downthread(self.ftpcon,  self.selectedinfos, all_header_combobox,'')
        else:
            dir= QtWidgets.QFileDialog.getExistingDirectory(self, "getExistingDirectory", "./")
            self.threadact = downthread(self.ftpcon, self.selectedinfos, all_header_combobox, dir)
        self.mythread = QThread()
        #thread.finished.connect(self.threadFinished)

        # 开始线程
        self.threadact.moveToThread(self.mythread)
        # # 连接信号

        self.mythread.started.connect(self.threadact.run)
        self.threadact.finished.connect(self.finishThread)
        self.mythread.start()
        self.threadact._signal.connect(self.updateProcessbar)  # 进程连接回传到GUI的事件
        self.threadact.noticesig.connect(self.runinfoShow)  # 运行信息信号
        self.threadact.curfilesig.connect(self.filenamShow)   # 当前下载文件
        self.stopButton.setEnabled(True)
        self.downloadButton.setEnabled(False)
        self.oncedownButton.setEnabled(False)
        #self.stopButton.clicked.connect(lambda: self.threadStoped(localFile))
        # self.filenamelabel.setText('开始下载:' + serverFile)
        # self.runinfoBrowser.append('保存至：' + localFile)

    def alldevicesBoxChanged(self):
        #print(self.tableinfos)
        self.selectedinfos={}
        if self.tableinfos:
            try:
                if self.alldevicesBox.currentText()=='全部':
                    #print([x for x in range(len(self.tableinfos))])
                    self.selectedinfos=self.tableinfos
                    self.setTableView()
                else:
                    i=0
                    for num in self.devices[self.alldevicesBox.currentText()].split(','):
                        self.selectedinfos[i]=self.tableinfos[int(num)-1]
                        i=i+1
                    self.setTableView()
            except KeyError:
                logging.exception('当前选择的设备获取到不存在的规则库索引')
                self.showDialog('warining','配置文件内容出错，请尝试修复')
############
    def showDevices(self):
        try:
            self.dbcursor.execute('select * from devices')
            alldevices=self.dbcursor.fetchall()
            for perdivece in alldevices:
                self.devices[perdivece[1]]=perdivece[2]
                self.alldevicesBox.addItem(perdivece[1])
        except:
            self.showDialog('warning','配置文件内容出错，请尝试修复')

    def stopThread(self):
        self.downloadButton.setEnabled(True)
        self.oncedownButton.setEnabled(True)
        self.stopButton.setEnabled(False)
        self.threadact.stop()
        self.mythread.quit()
        #self.mythread.wait()
    def finishThread(self):
        self.mythread.quit()
        self.downloadButton.setEnabled(True)
        self.oncedownButton.setEnabled(True)
        self.stopButton.setEnabled(False)
        self.activateWindow()
        self.showDialog('information','下载完成！')
        self.activateWindow()
        #self.mythread.wait(2)
        #self.mythread=''

    def filenamShow(self,serverFile):
        self.filenamelabel.setText('开始下载:' + serverFile)

    def runinfoShow(self,noticestr):

        self.runinfoBrowser.append(noticestr)

    def threadFinished(self):
        if not self._isStop:
            self.runinfoBrowser.append('下载完成' )
            self.downloadButton.setEnabled(True)

    def checkfinished(self,infosdict):
        self.mycheckthread.quit()
        self.tableinfos=infosdict
        self.alldevicesBox.setCurrentIndex(0)
        self.selectedinfos = self.tableinfos
        self.setTableView()
        self.checknewfileBotton.setEnabled(True)
        self.downloadButton.setEnabled(True)
        self.cleanoldButton.setEnabled(True)
        self.oncedownButton.setEnabled(True)

    def cleanoldButtonClicked(self):

        for num,fileinfo in self.tableinfos.items():
            if all_header_combobox[num].checkState()==2:
                foldername=fileinfo[4]
                fileregstr=fileinfo[0][2]
                newfilename=fileinfo[1]
                localexistfiles=os.listdir(foldername)

                ##判断目录中符合该规则库的文件名,并尝试去除掉新文件名
                filereg=re.compile(fileregstr)
                filelist=[]
                for localexistfile in localexistfiles:
                    filematch=filereg.match(localexistfile)
                    if filematch:
                        filelist.append(localexistfile)
                try:
                    filelist.remove(newfilename)
                except ValueError:
                    pass
                #print(filelist)
                for file in filelist:
                    filedir='/'.join([foldername,file])
                    try:
                        os.remove(filedir)
                        logging.info('成功删除'+filedir)
                    except:
                        pass
        self.showDialog('information','清理已完成')
                #print(filereg,newfilename,os.listdir(foldername))
            #print(all_header_combobox[num].checkState())
        #self.showDialog('information','开始清理')

    def updateProcessbar(self,datelen):
        #self.downloading=1
        self.progressBar.setValue(datelen)


class FTPSync:

    def __init__(self,ipaddr,port,user,password,encoding,localdir,dbcon):
            with open('config.xml', encoding='utf-8') as configfile:
                self.xmlcontentstr = configfile.read()
            self.serverdict = xmltodict.parse(self.xmlcontentstr)
            self.ipaddr = ipaddr
            self.port = port
            self.user = user
            self.passwd = password
            self.encoding = encoding
            self.dir = localdir
            self.dbcon=dbcon
            self.dbcursor=self.dbcon.cursor()
            self.ftp = FTP()
            self.files = []
            self.infos = []


    def ftpConnect(self):

        #print('*正在连接ftp服务器...')
        logging.info("start to connect ftp server")

        self.ftp.encoding = self.encoding
        self.ftp.connect(self.ipaddr, int(self.port))
        self.ftp.login(self.user, self.passwd)


    def getFile(self,signal):
        try:
            self.dbcursor.execute("select 规则库名,目录,文件名格式 from ftpfiles")
            featureinfos=self.dbcursor.fetchall()
            self.files=[]
            self.infos = []  # 用来存储当前目录下的文件/文件夹列表
            i=0
            for featureinfo in featureinfos:

                filename = self.getFileName(featureinfo)
                if filename == '':
                    continue
                self.files.append((filename, featureinfo))
                self.infos = []
                i=i+1
                signal.emit(int(i * 100 / len(featureinfos)))
            logging.info("get remote file's path success")
            return self.files
            # with open('ftpserverconfig.csv',encoding='gbk') as f:
            #     featureinfos = csv.reader(f)
            #     next(featureinfos)  # 排除掉第一行
            #     self.files=[]
            #     self.infos = []  # 用来存储当前目录下的文件/文件夹列表
            #     for featureinfo in featureinfos:
            #         filename = self.getFileName(featureinfo)
            #         if filename == '':
            #             continue
            #         self.files.append((filename, featureinfo))
            #         self.infos = []
            #     logging.info("get remote file's path success")
            #     return self.files
        except FileNotFoundError as E:
            logging.exception("config file not found"+str(E))

        except Exception as E:
            logging.exception("an unknown error occurred"+repr(E))


    def getNewestFile(self, files, featureinfo):
        #files, featureinfo=self.files[num]
        try:
            if files==[]:
                logging.error('该规则库未获取到文件信息：'+''.join(featureinfo))
                return 0,0,0,0
            if files[0][0].startswith('TopScanner') or files[0][0].startswith(
                    'sig'):  # 如果类似topscanner是包含在文件夹中的，无法下载文件夹，需要特殊处理
                topscannerlist = []
                self.ftp.dir(
                    ''.join([featureinfo[1], files[0][0]]), topscannerlist.append)
                topscannerlist.pop(0)
                for file in topscannerlist:
                    if file.split()[8].startswith(
                            'NPScanner') or file.split()[8].startswith('sig'):
                        filename = files[0][0] + '/' + file.split()[8]
                serverFile = ''.join([featureinfo[1], filename])
                localFile = ''.join(
                    [self.dir, featureinfo[0], '/', filename.split('/')[-1]])
            else:
                serverFile = ''.join([featureinfo[1], files[0][0]])
                localFile = ''.join(
                    [self.dir, featureinfo[0], '/', files[0][0].split('/')[-1]])
            localDir = ''.join([self.dir, featureinfo[0]])

            return (files[0][0].split('/')[-1],localFile,serverFile,localDir)

        except KeyboardInterrupt:
            if localFile:
                print('正在删除当前未完成的文件\n')
                logging.error("unexpect exit in downFile()")
                file.close()
                os.remove(localFile)
                logging.info("delete " + localFile + ' success')
                input('已删除' + localFile + '\n请按回车键退出...')
                exit(-1)
            exit(0)
        except Exception as E:
            logging.exception("an unknown error occurred"+ repr(E))
            print('下载出错了，提示错误信息为：' + repr(E))


    def downFile(self,localFile,serverFile,FILE):

        file = FILE
        size = self.ftp.size(serverFile)  # 进度条参数
        # widgets = ['Downloading: ', Percentage(), ' ',
        #            Bar(marker='#', left='[', right=']'),
        #            ' ', ETA(), ' ', FileTransferSpeed()]
        # progessBar = ProgressBar(widgets=widgets, maxval=size).start()
        bufsize = 1024
        datelen = 0

        # def file_write(data):  # 在ftp下载函数中调用
        #     nonlocal datelen
        #     datelen = datelen + len(data)
        #     file.write(data)
        #     # nonlocal progessBar
        #     # progessBar.update(datelen)

        # self.ftp.retrbinary("RETR " + serverFile, file_write, bufsize)
        file.close()
        #progessBar.finish()
        logging.info("sync " + serverFile + ' success')

    def getFileName(self, featureinfo):
        try:
            indexrecord = {}
            serverDIR = featureinfo[1]
            fileNameReg = re.compile(featureinfo[2])
            self.ftp.dir(serverDIR, self.infos.append)
            self.infos.pop(0)  # 去除第一条无用信息
        except KeyboardInterrupt:
            logging.error("unexpect exit in getFileName()")
            input('请按回车键退出...')
            exit(-1)
        except Exception:
            logging.exception("config file error"+''.join(featureinfo))
            print('当前处理出错，请检查配置文件对应行是否有错误：' + ''.join(featureinfo))
            return ''
        for info in self.infos:
            filename = info.split()[8]  # 当前目录下文件或文件夹名
            matchName = re.match(fileNameReg, filename)
            if matchName:
                try:  # 后续逻辑或许有点乱**代码还需修改**
                    indexrecord[filename] = datetime.strptime(
                        matchName.group(1), '%Y.%m.%d').date()
                except ValueError:
                    try:
                        indexrecord[filename] = datetime.strptime(
                            matchName.group(1), '%Y%m%d').date()
                    except ValueError:
                        try:  # 能运行到此就意味着匹配到的数据不是以日期为格式的了，需要特殊处理
                            if matchName.group(
                                    1) == 'ISP-IP-LISTS.bin':  # ISP列表直接下载即可
                                indexrecord[filename] = 'ISP-IP-LISTS.bin'
                            else:
                                nums = matchName.group(1).split('.')
                                # print(nums)
                                num = 0
                                for i in range(
                                        len(nums)):  # 将x.x.x.x这种形式的版本号处理为可以比较大小的方式
                                    num = num + int(nums[i]) * \
                                        1000000 / (10 ** (i * 2))
                                indexrecord[filename] = num
                                # indexrecord[filename] = datetime.strptime(matchName.group(1), '%H.%I.%M.%S').date()
                        except ValueError as E:
                            logging.exception(
                                "unexpect error in getFileName()")
                            print('当前匹配：', str(E))
                        except IndexError as E:
                            logging.exception(
                                "unable to match latest update file")
                            print('正则没有匹配到内容', repr(E))
        dates = sorted(
            indexrecord.items(),
            key=lambda x: x[1],
            reverse=True)  # 根据时间排序
        return dates

    def close(self):
        """
        正常退出
        :return:
        """
        logging.info("program finish")
        self.ftp.quit()
        input("请按回车键退出...")
        exit(0)




def main():
    logging.basicConfig(level=logging.INFO, filename='log', format="%(asctime)s %(levelname)s：%(funcName)s：%(message)s")
    # 适应高DPI设备
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    # 适应Windows缩放
    QtGui.QGuiApplication.setAttribute(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    app = QApplication(sys.argv)
    settingForm=setWindow()
    updateForm=updateWindow()
    instructionForm=instructionWindow()
    mainWindow = MainWindow()
    mainWindow.show()
    mainWindow.actionsetting.triggered.connect(settingForm.open)
    mainWindow.actioninstruction.triggered.connect(instructionForm.open)
    mainWindow.actionupdate.triggered.connect(updateForm.open)
    # mainWindow.editButton.clicked.connect(editWindow.open)

    sys.exit(app.exec())


if __name__ == "__main__":

    main()
